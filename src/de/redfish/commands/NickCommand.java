package de.redfish.commands;

import de.redfish.data.Data;
import de.redfish.data.NickHandler;
import de.redfish.data.UUIDFetcher;
import de.redfish.main.Nick;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.UUID;

public class NickCommand implements CommandExecutor {

    private static ArrayList<UUID> cooldown = new ArrayList<>();


    @Override
    public boolean onCommand(CommandSender cs , Command cmd , String s , String[] args) {

        if (cs instanceof Player) {
            Player p = (Player) cs;
            if(!cooldown.contains(p.getUniqueId()) || p.hasPermission("dynexmc.skip.cooldown")) {
                if (p.hasPermission("dynexmc.nick")) {
                    if (args.length == 0) {

                        NickHandler.randomNick(p);
                        cooldown.add(p.getUniqueId());

                    }else if (args.length >= 1) {
                        System.out.println();
                        try {
                            if (UUIDFetcher.getUUID(args[0]) == null) {
                                p.sendMessage(Data.prefix+"invalid UUID");
                                return true;
                            }
                        } catch (Exception e) {
                            p.sendMessage(Data.prefix+"invalid UUID");
                            return true;

                        }


                        NickHandler.Nick(p, args[0]);
                        cooldown.add(p.getUniqueId());

                        if (args.length == 2) {

                            if (args[1].equalsIgnoreCase("true")) {
                                NickHandler.showOwnSkin(p);
                            }

                        }
                    }else return false;
                    if(!p.hasPermission("dynexmc.skip.cooldown"))
                        new BukkitRunnable(){
                            @Override
                            public void run() {
                                cooldown.remove(p.getUniqueId());

                            }
                        }.runTaskLater(Nick.getInstance(),100);
                }
            }else p.sendMessage(Data.prefix+ ChatColor.RED + "Du musst warten bis dein" + ChatColor.YELLOW + " Cooldown " + ChatColor.RED +  "abgelaufen ist!");

        }

        return true;
    }
}

