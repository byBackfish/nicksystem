package de.redfish.commands;

import de.redfish.data.Data;
import de.redfish.data.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RealNameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs , Command cmd , String s , String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            if (p.hasPermission ( "dynexmc.realname" )) {

                p.sendMessage ( Data.prefix + " Der Echte-Name von " + ChatColor.YELLOW + args[0] + ChatColor.RED + " ist: " + ChatColor.GREEN + UUIDFetcher.getName ( Bukkit.getPlayer ( args[0] ).getUniqueId ( ) ) );

            }
        }

        return true;
    }
}
