package de.redfish.commands;

import de.redfish.data.NickHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UnNick implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender cs , Command cmd , String s , String[] args) {
        if (cs instanceof Player)
            if (cs.hasPermission ( "dynexmc.unnick" ) && NickHandler.isNicked((Player) cs)) {

                NickHandler.unNick((Player) cs);
            }

        return true;
    }
}
