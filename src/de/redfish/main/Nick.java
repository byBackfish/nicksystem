package de.redfish.main;

import de.redfish.commands.NickAdd;
import de.redfish.commands.NickCommand;
import de.redfish.commands.RealNameCommand;
import de.redfish.commands.UnNick;
import de.redfish.data.Data;
import de.redfish.data.NickEventsManager;
import de.redfish.database.Connection;
import de.redfish.database.MySql;
import de.redfish.database.NDBC.NDBM;
import de.redfish.events.DeadEvent;
import de.redfish.events.Debugger;
import de.redfish.events.InteractListener;
import de.redfish.events.JoinListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Nick extends JavaPlugin {
    private static MySql sql;
    private static Nick instance;

    public static Nick getInstance() {
        return instance;
    }

    public static MySql getMySql() {
        return sql;
    }

    @Override
    public void onEnable() {
        instance = this;
        loadCommands ( );
        loadEvents ( );
        loadConfig ( );

        Bukkit.getConsoleSender ( ).sendMessage ( Data.prefix + " Das Plugin wird gestartet...." );


    }

    @Override
    public void onDisable() {

        Bukkit.getConsoleSender ( ).sendMessage ( Data.prefix + " Das Plugin wird gestoppt...." );

    }


    private void loadCommands() {

        getCommand ( "nick" ).setExecutor ( new NickCommand ( ) );
        getCommand ( "unnick" ).setExecutor ( new UnNick ( ) );
        getCommand ( "addnick" ).setExecutor ( new NickAdd ( ) );
        getCommand ( "realname" ).setExecutor ( new RealNameCommand ( ) );

    }

    private void loadEvents() {
        Bukkit.getPluginManager ( ).registerEvents ( new InteractListener ( ) , this );
        Bukkit.getPluginManager ( ).registerEvents ( new JoinListener ( ) , this );
        Bukkit.getPluginManager ( ).registerEvents ( new DeadEvent ( ) , this );
        NickEventsManager.register(new Debugger());
    }

    private void establishDatabaseConnection(String user , String pwd , String ip4 , String db , int port) {

        Connection.getInstance ( ).setUpConnection ( user , pwd , ip4 , db , port );
        if (Connection.getInstance ( ).connectToDatabase ( )) {
            sql = Connection.getInstance ( ).getSQL ( );
            sql.debug_mode = false;
            System.out.println ( "Connected" );
        } else
            System.out.println ( "Could Not connect to database !" );

        NDBM.proofDataGrid ( );

    }


    private void loadConfig() {
        FileConfiguration cfg = getConfig ( );
        if (!cfg.getBoolean ( "Database.data.init" )) {
            cfg.set ( "Database.data.user" , "root" );
            cfg.set ( "Database.data.pwd" , "root" );
            cfg.set ( "Database.data.ip" , "localhost" );
            cfg.set ( "Database.data.database" , "nick" );
            cfg.set ( "Database.data.port" , "3306" );
            cfg.set ( "Database.data.init" , true );
            saveConfig ( );

        }
        establishDatabaseConnection (
                cfg.getString ( "Database.data.user" ) ,
                cfg.getString ( "Database.data.pwd" ) ,
                cfg.getString ( "Database.data.ip" ) ,
                cfg.getString ( "Database.data.database" ) ,
                Integer.parseInt ( cfg.getString ( "Database.data.port" ) )
        );


    }
}
