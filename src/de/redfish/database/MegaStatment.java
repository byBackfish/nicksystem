package de.redfish.database;

import java.util.ArrayList;
import java.util.List;

public class MegaStatment {
    private List<Statement> data;

    public MegaStatment(List<Statement> data) {
        this.data = data;

    }

    public List<Statement> getData() {
        return data;
    }

    public void setData(List<Statement> data) {
        this.data = data;
    }

    public ArrayList<Result> getRawData() {
        ArrayList<Result> statment = new ArrayList<> ( );
        for (Statement datas : data)
            statment.add ( new Result ( datas.getData ( ) ) );
        return statment;

    }

}
