package de.redfish.database;

public class Connection {

    private static Connection instance;

    private MySql sql;

    private Connection() {
        instance = this;

    }

    public static Connection getInstance() {
        if (instance == null)
            new Connection ( );
        return instance;

    }

    public void setUpConnection(String user , String pwd , String ip4 , String database , int port) {

        this.sql = new MySql ( user , pwd , ip4 , database , port );

    }

    public boolean connectToDatabase() {

        return this.sql.connect ( );

    }

    public MySql getSQL() {
        return this.sql;

    }

}
