package de.redfish.database;

import java.util.ArrayList;
import java.util.List;


public class Result {
    private String colum = "";

    public Result(String colum) {
        this.colum = colum;

    }

    public String getColum() {
        return colum;
    }

    public void setColum(String colum) {
        this.colum = colum;
    }


    public static List<Result> fromStringList(List<String> data) {

        if (data != null) {
            if (!data.isEmpty ( )) {
                List<Result> rs = new ArrayList<> ( );
                for (String datas : data)
                    rs.add ( new Result ( datas ) );

                return rs;
            } else
                return null;
        } else
            return null;

    }

}
