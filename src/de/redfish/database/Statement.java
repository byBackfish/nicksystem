package de.redfish.database;

public class Statement {
    private String attribut;
    private String data;

    public Statement(String at , String data) {
        this.data = data;
        this.attribut = at;


    }

    public String getAttribut() {
        return attribut;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


}
