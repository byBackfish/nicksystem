package de.redfish.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Table {
    public MySql database;
    public String table;

    public Table(String table , MySql database) {
        this.database = database;
        this.table = table;
        if (!database.tableExits ( table ))
            throw new NullPointerException ( ConsoleColors.RED_BOLD_BRIGHT
                    + "This Table does not Exists or yout are not connected with the right database !"
                    + ConsoleColors.RESET );

    }

    public String getAttribut(int row) {

        return database.getAttributsFromTable ( table ).get ( row );

    }

    public ArrayList<ArrayList<String>> getRow(Statement t) {

        return database.getRowFromTable ( table , t );

    }

    public void addObject(List<Statement> data) {
        HashMap<String, String> d2 = new HashMap<String, String> ( );
        for (Statement s : data)
            d2.put ( s.getAttribut ( ) , s.getData ( ) );
        database.addObjectToTable ( table , d2 );

    }

    public void removeObject(Statement row) {
        database.removeObjectFromTable ( table , row );

    }

    public void replace(List<Statement> replace , Statement which) {
        database.set ( replace , table , which );

    }

}
