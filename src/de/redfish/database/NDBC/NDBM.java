package de.redfish.database.NDBC;

import de.redfish.database.MySql;
import de.redfish.database.Result;
import de.redfish.database.Statement;
import de.redfish.main.Nick;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class NDBM {

    private static boolean dataGrid = false;
    private static MySql sql;


    public static void proofDataGrid() {
        if (!dataGrid) {
            try {
                sql = Nick.getMySql ( );
                if (!sql.tableExits ( "nick_data" ))
                    sql.createTable ( "nick_data" , Arrays.asList ( new Statement ( "varchar(63) primary key" , "uuid" ) ) , false );
                if (!sql.tableExits ( "nick_auto" ))
                    sql.createTable ( "nick_auto" , Arrays.asList ( new Statement ( "varchar(63) primary key" , "player" ) ) , false );
                dataGrid = true;
            } catch (Exception e) {
                dataGrid = false;
            }


        }


    }

    public static boolean addNametoNickList(String id) {
        if (dataGrid) {
            if (sql.getDataFromTable ( "nick_data" , Arrays.asList ( new Result ( "uuid" ) ) , Arrays.asList ( new Statement ( "uuid" , id ) ) ).isEmpty ( )) {
                sql.addObjectToTable ( "nick_data" , Arrays.asList ( new Statement ( "uuid" , id ) ) );
                return true;
            }


        }
        return false;

    }

    public static boolean addNametoNickList(UUID id) {
        if (dataGrid) {
            if (sql.getDataFromTable ( "nick_data" , Arrays.asList ( new Result ( "uuid" ) ) , Arrays.asList ( new Statement ( "uuid" , id.toString ( ) ) ) ).isEmpty ( )) {
                sql.addObjectToTable ( "nick_data" , Arrays.asList ( new Statement ( "uuid" , id.toString ( ) ) ) );
                return true;
            }


        }
        return false;

    }

    public static boolean removeNametoNickList(UUID id) {
        if (dataGrid) {
            if (!sql.getDataFromTable ( "nick_data" , Arrays.asList ( new Result ( "uuid" ) ) , Arrays.asList ( new Statement ( "uuid" , id.toString ( ) ) ) ).isEmpty ( )) {
                sql.addObjectToTable ( "nick_data" , Arrays.asList ( new Statement ( "uuid" , id.toString ( ) ) ) );
                return true;
            }


        }
        return false;
    }

    public static UUID getRandomAccount() {
        ArrayList<String> playerData = sql.getRandomRow ( "nick_data" );
        if (playerData.isEmpty ( ))
            throw new NullPointerException ( "No Account was added to Nick List" );
        return UUID.fromString ( playerData.get ( 0 ) );

    }

    public static boolean activateAutoNick(Player p) {
        if (dataGrid) {
            UUID id = p.getUniqueId ( );
            if (sql.getDataFromTable ( "nick_auto" , Arrays.asList ( new Result ( "player" ) ) , Arrays.asList ( new Statement ( "player" , id.toString ( ) ) ) ).isEmpty ( )) {
                sql.addObjectToTable ( "nick_auto" , Arrays.asList ( new Statement ( "player" , id.toString ( ) ) ) );
                return true;
            }


        }
        return false;

    }

    public static boolean deactivateAutoNick(Player p) {
        if (dataGrid) {
            UUID id = p.getUniqueId ( );
            if (!sql.getDataFromTable ( "nick_auto" , Arrays.asList ( new Result ( "player" ) ) , Arrays.asList ( new Statement ( "player" , id.toString ( ) ) ) ).isEmpty ( )) {
                sql.removeObjectFromTable ( "nick_auto" , Arrays.asList ( new Statement ( "player" , id.toString ( ) ) ) );
                return true;
            }


        }
        return false;

    }

    public static boolean playerIsAutoNicked(Player p) {
        if (dataGrid)
            return !sql.getDataFromTable ( "nick_auto" , Arrays.asList ( new Result ( "player" ) ) , Arrays.asList ( new Statement ( "player" , p.getUniqueId ( ).toString ( ) ) ) ).isEmpty ( );
        return false;

    }


}
