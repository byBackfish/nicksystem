package de.redfish.database;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MySql {

    private String user = "root";
    private String pwd = "root";
    private String adress = "localhost";
    private String database = "plugin";
    private int port = 3306;
    private Connection con;
    public boolean debug_mode;

    public MySql(String user , String pwd , String adress , String database , int port) {
        this.user = user;
        this.pwd = pwd;
        this.adress = adress;
        this.database = database;
        this.port = port;

    }

    public Connection getConnection() {

        return con;
    }

    public boolean automCommit(boolean b) {
        if (isConnected ( ))
            try {
                getConnection ( ).setAutoCommit ( b );
                return true;
            } catch (SQLException e1) {

                e1.printStackTrace ( );
                return false;
            }
        return false;

    }

    public boolean Commit() {
        if (isConnected ( ))
            try {
                getConnection ( ).commit ( );
                return true;
            } catch (SQLException e1) {

                e1.printStackTrace ( );
                return false;
            }
        return false;

    }

    public void debug(boolean b) {

        this.debug_mode = b;
    }

    public boolean connect() {
        if (!isConnected ( )) {
            try {
                con = DriverManager.getConnection ( "jdbc:mysql://" + adress + ":" + port + "/" + database , user , pwd );
                ddl ( "SET SQL_SAFE_UPDATES = 0" );
            } catch (SQLException e) {

            }
        }
        return isConnected ( );
    }

    public boolean isConnected() {
        return !(con == null);

    }

    public void disconnect() {
        if (isConnected ( ))
            try {
                con.close ( );
            } catch (SQLException e) {

                e.printStackTrace ( );
            }

    }

    private void ddl(String sql) throws SQLException {
        if (debug_mode)
            System.out.println ( "ddl : [ " + sql + " ]" );
        if (isConnected ( )) {

            PreparedStatement ps = con.prepareStatement ( sql );
            ps.executeUpdate ( );

        }

    }

    private ResultSet dml(String sql) {
        if (debug_mode)
            System.out.println ( "dml : [ " + sql + " ]" );
        if (isConnected ( ))
            try {
                PreparedStatement ps = con.prepareStatement ( sql );
                return ps.executeQuery ( );
            } catch (SQLException e) {

                e.printStackTrace ( );
            }
        return null;

    }

    public boolean createTable(String name , List<String> attributs) {
        if (isConnected ( )) {

            String attribut = "";
            if (attributs == null)
                return false;
            for (int i = 0; i < attributs.size ( ); i++)
                if (!(i == attributs.size ( ) - 1))
                    attribut += attributs.get ( i ) + ",";
                else
                    attribut += attributs.get ( i );

            try {
                ddl ( "create table " + name + "(" + attribut + ")" );
                return true;
            } catch (SQLException e) {
                e.printStackTrace ( );
                return false;
            }

        } else
            return false;

    }

    public boolean createTable(String name , List<Statement> attributs ,
                               boolean IGNOREDoubleMethodExpectionWithoutItIDKkWhy) {
        if (isConnected ( )) {

            String attribut = "";
            if (attributs.isEmpty ( ))
                return false;
            for (int i = 0; i < attributs.size ( ); i++)
                if (!(i == attributs.size ( ) - 1))
                    attribut += attributs.get ( i ).getData ( ) + " " + attributs.get ( i ).getAttribut ( ) + ",";
                else
                    attribut += attributs.get ( i ).getData ( ) + " " + attributs.get ( i ).getAttribut ( );

            try {
                ddl ( "create table " + name + "(" + attribut + ")" );
                return true;
            } catch (SQLException e) {
                e.printStackTrace ( );
                return false;
            }

        } else
            return false;

    }

    public boolean createTable(String name , List<Statement> attributs , List<Result> index) {
        if (isConnected ( )) {

            String attribut = "";
            if (attributs.isEmpty ( ))
                return false;
            for (int i = 0; i < attributs.size ( ); i++)
                if (i != attributs.size ( ) - 1)
                    attribut += attributs.get ( i ).getData ( ) + " " + attributs.get ( i ).getAttribut ( ) + ",";
                else
                    attribut += attributs.get ( i ).getData ( ) + " " + attributs.get ( i ).getAttribut ( );
            if (index != null)
                if (!index.isEmpty ( )) {
                    attribut += ", index(";
                    for (int i = 0; i < index.size ( ); i++)
                        if (i != index.size ( ) - 1)
                            attribut += index.get ( i ).getColum ( ) + ",";
                        else
                            attribut += index.get ( i ).getColum ( );
                    attribut += ")";


                }

            try {
                ddl ( "create table " + name + "(" + attribut + ")" );
                return true;
            } catch (SQLException e) {
                e.printStackTrace ( );
                return false;
            }

        } else
            return false;

    }


    public ArrayList<ArrayList<String>> getDataFromTable(String name , String attribut) {

        if (isConnected ( )) {
            ResultSet rs = dml ( "select " + attribut + " from " + name );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }


    public ArrayList<ArrayList<String>> getDataFromTable(String name , List<Result> colums , boolean I) {

        if (isConnected ( )) {
            String attribut = "";
            for (int i = 0; i < colums.size ( ) - 1; i++)
                attribut += ", " + colums.get ( i );
            attribut += " " + colums.get ( colums.size ( ) - 1 );


            ResultSet rs = dml ( "select " + attribut + " from " + name );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    public ArrayList<ArrayList<String>> getDataFromTable(String table , List<Result> what , List<Statement> where) {
        if (isConnected ( ) && !what.isEmpty ( )) {

            String rep = "";
            for (Result r : what)
                rep += r.getColum ( ) + " ,";
            rep = rep.substring ( 0 , rep.length ( ) - 1 );
            String con = "";
            if (where != null) {
                if (!where.isEmpty ( )) {
                    con += "where ";
                    for (Statement s : where)
                        con += s.getAttribut ( ) + " = '" + s.getData ( ) + "' AND ";
                    con = con.substring ( 0 , con.length ( ) - 4 );

                }
            }

            ResultSet rs = dml ( "select " + rep + " from " + table + " " + con );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    public ArrayList<ArrayList<String>> getDataFromTable(String table , ArrayList<Result> what , List<Statement> where) {
        if (isConnected ( ) && !what.isEmpty ( )) {

            String rep = "";
            for (Result r : what)
                rep += r.getColum ( ) + " ,";
            rep = rep.substring ( 0 , rep.length ( ) - 1 );
            String con = "";
            if (where != null) {
                if (!where.isEmpty ( )) {
                    con += "where ";
                    for (Statement s : where)
                        con += s.getAttribut ( ) + " = '" + s.getData ( ) + "' AND";
                    con = con.substring ( 0 , con.length ( ) - 4 );

                }
            }

            ResultSet rs = dml ( "select " + rep + " from " + table + " " + con );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    public ArrayList<ArrayList<String>> getDataFromTable(String name , List<Statement> attributs) {

        if (isConnected ( )) {

            ResultSet rs;
            if (attributs.isEmpty ( ))
                rs = dml ( "select * from " + name );
            else {
                String condition = "";
                for (Statement t : attributs)
                    condition += t.getAttribut ( ) + " = '" + t.getData ( ) + "' AND ";
                condition = condition.substring ( 0 , condition.length ( ) - 5 );
                rs = dml ( "select * from " + name + " where " + condition );

            }
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    public ArrayList<ArrayList<String>> getRowFromTable(String name , Statement t) {

        if (isConnected ( )) {
            ResultSet rs = dml ( "select * from " + name + " where " + t.getAttribut ( ) + " = '" + t.getData ( ) + "'" );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    /*
     * public boolean removeValueFromTable(String table, String primarykey) { if
     * (!isConnected()) return false; dml("delete from "+table+" where ");
     *
     *
     * }
     */
    public int getPrimaryKey(String table) {

        ResultSet rs = dml ( "describe " + table );
        ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
        int index = 1;
        try {
            while (rs.next ( )) {
                ArrayList<String> meta = new ArrayList<String> ( );
                for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                    if (rs.getString ( i ) != null && rs.getString ( i ).equalsIgnoreCase ( "PRI" ))
                        return index;

                }
                statment.add ( meta );
                index++;

            }
        } catch (SQLException e) {

            e.printStackTrace ( );
        }
        return -1;

    }

    public String getPrimaryAttribut(String table) {

        ResultSet rs = dml ( "describe " + table );
        ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );

        try {
            while (rs.next ( )) {
                ArrayList<String> meta = new ArrayList<String> ( );
                for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                    if (rs.getString ( i ) != null && rs.getString ( i ).equalsIgnoreCase ( "PRI" ))
                        return rs.getString ( 1 );

                }
                statment.add ( meta );

            }
        } catch (SQLException e) {

            e.printStackTrace ( );
        }
        return null;

    }

    @Deprecated
    public boolean removeObjectFromTable(String table , String primarykey) {
        if (isConnected ( )) {
            try {
                ddl ( "delete from " + table + " where " + getPrimaryAttribut ( table ) + " = '" + primarykey + "'" );
            } catch (SQLException e) {

                e.printStackTrace ( );
            }
            return true;

        }
        return false;

    }

    public boolean removeObjectFromTable(String table , Statement row) {
        if (isConnected ( )) {
            try {
                ddl ( "delete from " + table + " where " + row.getAttribut ( ) + " = '" + row.getData ( ) + "'" );
            } catch (SQLException e) {

                e.printStackTrace ( );
            }
            return true;

        }
        return false;

    }

    public boolean removeObjectFromTable(String table , List<Statement> multiconditions) {
        if (isConnected ( )) {
            try {
                ddl ( "delete from " + table + multiConditions ( multiconditions ) );
            } catch (SQLException e) {

                e.printStackTrace ( );
            }
            return true;

        }
        return false;

    }

    public boolean addObjectToTable(String table , List<Statement> data) {
        String attributs = "";
        String data_at = "";
        for (Statement t : data) {
            attributs += t.getAttribut ( ) + ",";
            data_at += "'" + t.getData ( ) + "',";
        }
        attributs = attributs.substring ( 0 , attributs.length ( ) - 1 );
        data_at = data_at.substring ( 0 , data_at.length ( ) - 1 );
        try {
            ddl ( "insert into " + table + " (" + attributs + ") values(" + data_at + ")" );
            return true;
        } catch (SQLException e) {
            e.printStackTrace ( );
            return false;
        }
    }

    public boolean addObjectToTable(String table , HashMap<String, String> data) {
        if (isConnected ( )) {
            if (data == null)
                return false;
            if (data.size ( ) == 0)
                return false;
            ArrayList<String> includ = new ArrayList<String> ( );
            includ.addAll ( data.keySet ( ) );
            String attributs = "";
            for (String attribut : includ)
                attributs += "," + attribut;
            attributs = attributs.substring ( 1 );
            String datas = "";

            for (String attribut : includ)
                datas += ",'" + data.get ( attribut ) + "'";
            datas = datas.substring ( 1 );
            try {
                ddl ( "insert into " + table + " (" + attributs + ") values(" + datas + ")" );
                return true;
            } catch (SQLException e) {

                e.printStackTrace ( );
            }

        }
        return false;

    }

    public ArrayList<String> getAttributsFromTable(String table) {
        if (isConnected ( )) {
            ResultSet rs = dml ( "describe " + table );
            ArrayList<String> statment = new ArrayList<String> ( );
            try {
                while (rs.next ( )) {

                    statment.add ( rs.getString ( 1 ) );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            return statment;
        } else
            return null;

    }

    public boolean deleteTable(String table) {
        if (isConnected ( ))

            try {
                ddl ( "drop " + table );
                return true;
            } catch (SQLException e) {

                e.printStackTrace ( );
                return false;
            }
        return false;

    }

    public ArrayList<String> getRandomRow(String table) {
        if (isConnected ( )) {

            ResultSet rs = dml ( "SELECT * FROM " + table + " ORDER BY RAND() LIMIT 1" );
            ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
            try {
                while (rs.next ( )) {
                    ArrayList<String> meta = new ArrayList<String> ( );
                    for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                        meta.add ( rs.getString ( i ) );

                    }
                    statment.add ( meta );

                }
            } catch (SQLException e) {
                e.printStackTrace ( );
            }
            if (!statment.isEmpty ( ))
                return statment.get ( 0 );
            else return new ArrayList<String> ( );
        } else
            return null;


    }

    public boolean replace(String tabel , List<Statement> repla , List<Statement> what) {
        if (isConnected ( ) && !repla.isEmpty ( )) {
            String rep = "";
            for (Statement s : repla)
                rep += " " + s.getAttribut ( ) + " = '" + s.getData ( ) + "' ,";
            rep = rep.substring ( 0 , rep.length ( ) - 1 );
            String con = "";
            if (what != null) {
                if (!what.isEmpty ( )) {
                    con += "where ";
                    for (Statement s : what)
                        con += s.getAttribut ( ) + " = '" + s.getData ( ) + "' AND";
                    con = con.substring ( 0 , con.length ( ) - 4 );

                }
            }

            try {
                ddl ( "update " + tabel + " set " + rep + con );
            } catch (SQLException e) {
                return false;
            }

        }
        return false;

    }

    public boolean createDatabase(String name) {
        if (isConnected ( ))

            try {
                ddl ( "create database " + name );
                return true;
            } catch (SQLException e) {

                e.printStackTrace ( );
                return false;
            }
        return false;

    }

    public boolean deleteDatabase(String name) {
        if (isConnected ( ))
            try {
                ddl ( "drop " + name );
                return true;
            } catch (SQLException e) {

                e.printStackTrace ( );
                return false;
            }

        return false;
    }

    public boolean switchDatabase(String name) throws ConnectException {
        this.disconnect ( );
        String olddatabase = this.database;
        this.database = name;
        if (!isConnected ( )) {
            this.database = olddatabase;
            if (isConnected ( ))

                throw new ConnectException ( "The api was not able to connect to your old database !" );

            return false;

        } else
            return true;

    }


    public ArrayList<ArrayList<String>> dataFromResultSet(ResultSet rs) {

        ArrayList<ArrayList<String>> statment = new ArrayList<ArrayList<String>> ( );
        try {
            while (rs.next ( )) {
                ArrayList<String> meta = new ArrayList<String> ( );
                for (int i = 1; i < rs.getMetaData ( ).getColumnCount ( ) + 1; i++) {

                    meta.add ( rs.getString ( i ) );

                }
                statment.add ( meta );

            }
        } catch (SQLException e) {
            e.printStackTrace ( );
        }
        return statment;

    }

    public ArrayList<ArrayList<String>> getStatmentfromTable(String table , Statement where) {
        if (isConnected ( )) {
            return dataFromResultSet (
                    dml ( "select * from " + table + " WHERE " + where.getAttribut ( ) + " = '" + where.getData ( ) + "'" ) );
        }
        return new ArrayList<ArrayList<String>> ( );

    }

    public boolean HashMapToTable(HashMap<String, String> data , String table , String KEYNAME , String VALUENAME) {
        if (isConnected ( )) {

            ArrayList<String> t = new ArrayList<String> ( );
            t.add ( VALUENAME + " varchar(63)" );
            t.add ( KEYNAME + " varchar(63) primary key" );
            createTable ( table , t );

            for (String primary : data.keySet ( )) {
                String value = data.get ( primary );
                HashMap<String, String> d2 = new HashMap<String, String> ( );
                d2.put ( KEYNAME , primary );
                d2.put ( VALUENAME , value );
                if (!addObjectToTable ( table , d2 ))
                    return false;

            }
            return true;
        }

        return false;

    }

    public HashMap<String, String> tableToHahMap(String table) {
        if (isConnected ( )) {
            HashMap<String, String> statment = new HashMap<String, String> ( );

            for (ArrayList<String> data : getDataFromTable ( table , "*" ))
                statment.put ( data.get ( 0 ) , data.get ( 1 ) );

            return statment;
        }
        return new HashMap<String, String> ( );

    }

    public boolean set(List<Statement> data , String table , Statement replacestatment) {
        if (isConnected ( )) {
            try {
                for (Statement t : data)
                    ddl ( "UPDATE " + table + " SET " + t.getAttribut ( ) + " = '" + t.getData ( ) + "' WHERE "
                            + replacestatment.getAttribut ( ) + " = '" + replacestatment.getData ( ) + "'" );
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ( );
            }

        }

        return false;
    }

    public boolean set(List<Statement> data , String table , List<Statement> replacestatments) {
        if (isConnected ( )) {
            try {
                String conditions = " WHERE";
                for (Statement s : replacestatments)
                    conditions += " " + s.getAttribut ( ) + " = '" + s.getData ( ) + "' AND";
                conditions.substring ( 0 , conditions.length ( ) - 3 );
                for (Statement t : data)
                    ddl ( "UPDATE " + table + " SET " + t.getAttribut ( ) + " = '" + t.getData ( ) + conditions );
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ( );
            }

        }

        return false;
    }

    private String multiConditions(List<Statement> Conditions) {
        if (Conditions.size ( ) == 0)
            return "";
        String conditions = " WHERE";
        for (Statement s : Conditions)
            conditions += " " + s.getAttribut ( ) + " = '" + s.getData ( ) + "' AND";
        conditions = conditions.substring ( 0 , conditions.length ( ) - 3 );

        return conditions;

    }

    public boolean tableExits(String tabel) {
        if (isConnected ( ))
            try {
                return dml ( "SHOW TABLES LIKE '" + tabel + "'" ).next ( );
            } catch (SQLException e) {
                e.printStackTrace ( );
                return false;
            }
        return false;

    }

    @Deprecated
    public void DDL(String cmd) {
        try {

            ddl ( cmd );
        } catch (SQLException e) {

            e.printStackTrace ( );
        }

    }

    @Deprecated
    public void DML(String cmd) {

        dml ( cmd );

    }

}
