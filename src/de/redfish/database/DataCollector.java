package de.redfish.database;

import java.util.List;


public class DataCollector {
    private int indexsFinshed = 0;
    private String compiledString;
    private final int limit;
    private final MySql sql;
    private final List<Result> colums;
    private final String table;

    @SuppressWarnings("deprecation")
    public DataCollector(List<Result> colums , String table , int limit , MySql sql) {
        this.limit = limit;
        this.sql = sql;
        this.table = table;
        this.colums = colums;
        //sql.DDL("SET sql_log_bin = 0");
        reset ( );
        sql.automCommit ( false );

    }

    private void addMegaStatment(MegaStatment s) {
        compiledString += "(";
        for (Result r : s.getRawData ( ))
            compiledString += "'" + r.getColum ( ) + "' ,";
        compiledString = compiledString.substring ( 0 , compiledString.length ( ) - 1 );
        compiledString += "),";


    }

    public void addData(MegaStatment s) {
        if (indexsFinshed <= limit) {
            addMegaStatment ( s );

        } else {

            sendSQL ( );
            reset ( );
            indexsFinshed = 0;
            addMegaStatment ( s );
        }
        indexsFinshed++;


    }

    private void reset() {
        String specData = "";
        for (Result rs : colums)
            specData += rs.getColum ( ) + " ,";
        specData = specData.substring ( 0 , specData.length ( ) - 1 );

        compiledString = "insert into " + table + " (" + specData + ")VALUES";

    }

    public void complete() {
        sendSQL ( );
        sql.Commit ( );
        sql.automCommit ( false );
        //sql.DDL("SET sql_log_bin = 1");


    }

    @SuppressWarnings("deprecation")
    private void sendSQL() {


        compiledString = compiledString.substring ( 0 , compiledString.length ( ) - 1 );
        compiledString += ";";


        sql.DDL ( compiledString );


    }


}
