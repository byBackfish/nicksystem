package de.redfish.data;

import org.bukkit.entity.Player;

import java.util.ArrayList;

public class NickEventsManager {

    private static ArrayList<NickEvents> observer = new ArrayList<>();

    public static void register(NickEvents e){
        observer.add(e);


    }
    protected static void onUnNick(Player p , String name){
        for(NickEvents n : observer)
            n.onUnNick(p,name);

    }
    protected static void onNick(Player p , String name){
        for(NickEvents n : observer)
            n.onNick(p,name);

    }
}
