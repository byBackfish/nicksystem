package de.redfish.data;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.dynexapi.mysql.api.ClanManager;
import de.dynexapi.mysql.api.DyePlayer;
import de.redfish.database.NDBC.NDBM;
import de.redfish.main.Nick;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Warning;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

public class NickHandler {

    private static final HashMap<Player, Location> ploc = new HashMap<> ( );
    private static final HashMap<Player, Double> phealth = new HashMap<> ( );
    private static final HashMap<Player, Integer> pfood = new HashMap<> ( );
    public static HashMap<Player, Boolean> pforce = new HashMap<> ( );

    private static void sendPacket(Packet packet) {
        for (Player all : Bukkit.getOnlinePlayers ( ))

            ((CraftPlayer) all).getHandle ( ).playerConnection.sendPacket ( packet );

    }


    private static EntityPlayer changePlayerName(Player p , String name) throws NoSuchFieldException, IllegalAccessException {
        EntityPlayer ep = ((CraftPlayer) p).getHandle ( );
        GameProfile gp = ep.getProfile ( );
        Field playerName = gp.getClass ( ).getDeclaredField ( "name" );
        playerName.setAccessible ( true );
        playerName.set ( gp , name );
        return ep;

    }



    private static void respawn(Player p) {
        new BukkitRunnable ( ) {
            @Override
            public void run() {
                for (Player all : Bukkit.getOnlinePlayers ( ))
                    all.hidePlayer ( p );
            }
        }.runTask ( Nick.getInstance ( ) );

        new BukkitRunnable ( ) {
            @Override
            public void run() {
                for (Player all : Bukkit.getOnlinePlayers ( ))
                    all.showPlayer ( p );

            }
        }.runTaskLater ( Nick.getInstance ( ) , 2 );
    }

    protected static void setSkin(Player p , String name) {

        GameProfile gp = ((CraftPlayer) p).getProfile ( );

        gp.getProperties ( ).removeAll ( "textures" );

        String id = UUIDFetcher.getUUID ( name ).toString ( ).replaceAll ( "-" , "" );

        Skin skin = new Skin ( id );
        gp.getProperties ( ).clear ( );
        gp.getProperties ( ).put ( skin.getSkinName ( ) , new Property ( skin.getSkinName ( ) , skin.getSkinValue ( ) , skin.getSkinSignatur ( ) ) );


        respawn ( p );
    }

    protected static void setNameTag(Player p , String name) {


        final String RealName = ((CraftPlayer) p).getHandle ( ).getName ( );
        try {
            for (Player all : Bukkit.getOnlinePlayers ( ))
                if (!all.getName ( ).equalsIgnoreCase ( p.getName ( ) ))
                    new PacketPlayOutPlayerInfo ( PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER , ((CraftPlayer) p).getHandle ( ) );
            EntityPlayer ep = changePlayerName ( p , name );
            for (Player all : Bukkit.getOnlinePlayers ( ))
                if (!all.getName ( ).equalsIgnoreCase ( p.getName ( ) ))
                    ((CraftPlayer) all).getHandle ( ).playerConnection.sendPacket (
                            new PacketPlayOutPlayerInfo ( PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER , ep ) );

            // changePlayerName(p,RealName);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace ( );
        }


    }


    public static void showOwnSkin(Player p) {

        ploc.put ( p , p.getLocation ( ) );
        phealth.put ( p , p.getHealth ( ) );
        pfood.put ( p , p.getFoodLevel ( ) );

        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy ( p.getEntityId ( ) );
        sendPacket ( destroy );
        PacketPlayOutPlayerInfo removetab = new PacketPlayOutPlayerInfo ( PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER , ((CraftPlayer) p).getHandle ( ) );
        sendPacket ( removetab );

        p.setHealth ( 0 );


        new BukkitRunnable ( ) {
            @Override
            public void run() {
                p.spigot ( ).respawn ( );

                p.teleport ( ploc.get ( p ) );
                p.setHealth ( phealth.get ( p ) );
                p.setFoodLevel ( pfood.get ( p ) );

                pfood.remove ( p );
                ploc.remove ( p );
                phealth.remove ( p );


                PacketPlayOutPlayerInfo tabadd = new PacketPlayOutPlayerInfo ( PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER , ((CraftPlayer) p).getHandle ( ) );
                sendPacket ( tabadd );
                PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn ( ((CraftPlayer) p).getHandle ( ) );
                for (Player all : Bukkit.getOnlinePlayers ( )) {
                    if (!all.getName ( ).equals ( p.getName ( ) )) {
                        ((CraftPlayer) all).getHandle ( ).playerConnection.sendPacket ( spawn );
                    }
                }
            }
        }.runTaskLater ( Nick.getInstance ( ) , 4 );
    }


    public static boolean isNicked(Player p) {
        return !UUIDFetcher.getUUID ( p.getName ( ) ).toString ( ).equals ( p.getUniqueId ( ).toString ( ) );

    }

    public static String getRealName(Player p) {
        return UUIDFetcher.getName ( Bukkit.getPlayer ( p.getName ( ) ).getUniqueId ( ) );

    }

    public static void randomNick(Player p) {
        String name = UUIDFetcher.getName ( NDBM.getRandomAccount ( ) );
        setFullNick ( p ,  name);
        NickEventsManager.onNick(p,name);

    }

    @Warning(reason = "Don't touch it if it works !!!!!!!!! HIGH RISK")
    public static void unNick(Player player) {
        String name = UUIDFetcher.getName ( player.getUniqueId());
        setFullNick ( player , name );
        NickEventsManager.onUnNick(player,name);

        player.sendMessage ( Data.prefix + ChatColor.YELLOW + "Dein Nickname wurde " + ChatColor.RED + "Entfernt!" );

    }
    public static void Nick(Player player , String name){
        setFullNick(player,name);
        NickEventsManager.onNick(player,name);

    }

    private static void setFullNick(Player player , String name) {
        DyePlayer dyePlayer = new DyePlayer ( name );

        pforce.put ( player , true );

        setSkin ( player , name );

        if (dyePlayer.isInGroup ( )) {
            if(!ClanManager.playerInClan(name)) {
                player.setPlayerListName(dyePlayer.getColor() + dyePlayer.getRang() + " §8x " + dyePlayer.getColor() + name);
                player.setDisplayName(dyePlayer.getColor() + dyePlayer.getRang() + " §8x " + dyePlayer.getColor() + name);
            }else{
                player.setPlayerListName(dyePlayer.getColor() + dyePlayer.getRang() + " §8x " + dyePlayer.getColor() + name + " §7[§e" + dyePlayer.getClanTag() + "§7]");
                player.setDisplayName(dyePlayer.getColor() + dyePlayer.getRang() + " §8x " + dyePlayer.getColor() + name + " §7[§e" + dyePlayer.getClanTag() + "§7]");


            }
        } else {
            player.setDisplayName ( "§7Spieler §8x §7" + name );
            player.setPlayerListName ( "§7Spieler §8x §7" + name );
        }

        setNameTag ( player , name );
        player.sendMessage ( Data.prefix + ChatColor.GREEN + "Du bist nun als " + ChatColor.YELLOW + name + ChatColor.GREEN + " genickt!" );


        // respawn ( player );


    }

    public static void toggleAutoNick(Player p) {
        ItemStack i = p.getItemInHand ( );
        if (!hasAutoNick ( p )) {
            NDBM.activateAutoNick ( p );
            Data.givePlayerItem ( p );
            String name = UUIDFetcher.getName ( NDBM.getRandomAccount ( ) );
            randomNick ( p );
            p.sendMessage ( Data.prefix + ChatColor.GREEN + "Du hast das " + ChatColor.YELLOW + "AutoNick" + ChatColor.GREEN + " Feature Aktiviert!" );
        } else {
            NDBM.deactivateAutoNick ( p );
            Data.givePlayerItem ( p );
            unNick ( p );


            p.sendMessage ( Data.prefix + ChatColor.RED + "Du hast das " + ChatColor.YELLOW + "AutoNick" + ChatColor.RED + " Feature Deaktivert!" );

            return;
        }


    }

    public static boolean hasAutoNick(Player p) {

        return NDBM.playerIsAutoNicked ( p );

    }


}
