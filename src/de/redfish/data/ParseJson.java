package de.redfish.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ParseJson {

    protected static String getSkinValue(String json) {

        JsonObject jsonObject = new JsonParser ( ).parse ( json ).getAsJsonObject ( );


        JsonArray arr = jsonObject.getAsJsonArray ( "properties" );
        for (int i = 0; i < arr.size ( ); i++) {
            String value = arr.get ( i ).getAsJsonObject ( ).get ( "value" ).getAsString ( );


            return value;
        }


        return "Not Found";
    }

    protected static String getSkinSignature(String json) {

        JsonObject jsonObject = new JsonParser ( ).parse ( json ).getAsJsonObject ( );


        JsonArray arr = jsonObject.getAsJsonArray ( "properties" );
        for (int i = 0; i < arr.size ( ); i++) {
            String value = arr.get ( i ).getAsJsonObject ( ).get ( "signature" ).getAsString ( );


            return value;
        }


        return "Not Found";
    }
}
