package de.redfish.data;

import de.redfish.database.NDBC.NDBM;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Data {

    public static String prefix = "§8| §e§lNick §8» §r";

    public static void givePlayerItem(Player p) {
        if (NDBM.playerIsAutoNicked ( p )) {
            Material type;
            ItemStack i = new ItemStack ( Material.NAME_TAG );
            ItemMeta im = i.getItemMeta ( );
            im.setDisplayName ( ChatColor.YELLOW + "AutoNick" + ChatColor.GRAY + " | " + ChatColor.GREEN + "Aktiviert" );
            i.setItemMeta ( im );

            p.getInventory ( ).setItem ( 3 , i );
        } else if (!NDBM.playerIsAutoNicked ( p )) {
            Material type;
            ItemStack i = new ItemStack ( Material.NAME_TAG );
            ItemMeta im = i.getItemMeta ( );
            im.setDisplayName ( ChatColor.YELLOW + "AutoNick" + ChatColor.GRAY + " | " + ChatColor.RED + "Deaktiviert" );
            i.setItemMeta ( im );

            p.getInventory ( ).setItem ( 3 , i );

        } else {

            Material type;
            ItemStack i = new ItemStack ( Material.NAME_TAG );
            ItemMeta im = i.getItemMeta ( );
            im.setDisplayName ( ChatColor.YELLOW + "AutoNick" + ChatColor.GRAY + " | " + ChatColor.GRAY + "Unknown" );
            i.setItemMeta ( im );

            p.getInventory ( ).setItem ( 3 , i );

        }

    }


}