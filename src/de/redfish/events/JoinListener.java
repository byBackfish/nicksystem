package de.redfish.events;

import de.redfish.data.Data;
import de.redfish.data.NickHandler;
import de.redfish.database.NDBC.NDBM;
import de.redfish.main.Nick;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(org.bukkit.event.player.PlayerJoinEvent e) {

        e.setJoinMessage ( null );
        for(Player all : Bukkit.getOnlinePlayers ( )){
            all.hidePlayer ( e.getPlayer () );
        }
        if (NDBM.playerIsAutoNicked ( e.getPlayer ( ) )) {
            for(Method m : NickHandler.class.getMethods())
                if(m.getName().equals("setFullNick")) {
                    try {
                        m.invoke(e.getPlayer(),e.getPlayer().getName());
                    } catch (IllegalAccessException illegalAccessException) {
                        illegalAccessException.printStackTrace();
                    } catch (InvocationTargetException invocationTargetException) {
                        invocationTargetException.printStackTrace();
                    }
                }
            new BukkitRunnable ( ) {
                public void run() {
                    NickHandler.randomNick ( e.getPlayer ( ) );
                    for(Player pp : Bukkit.getOnlinePlayers ()){
                        pp.showPlayer ( e.getPlayer () );
                    }
                }
            }.runTaskLater ( Nick.getInstance ( ) , 4 );


        }


    }


}
