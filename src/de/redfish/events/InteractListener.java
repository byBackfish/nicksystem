package de.redfish.events;

import de.redfish.data.Data;
import de.redfish.data.NickHandler;
import de.redfish.main.Nick;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.UUID;

public class InteractListener implements Listener {

    private static ArrayList<UUID> cooldown = new ArrayList<>();

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getPlayer ( ).hasPermission ( "dynexmc.autonick" )) {
            if(!cooldown.contains(e.getPlayer().getUniqueId()) || e.getPlayer().hasPermission("dynexmc.skip.cooldown")) {
                ItemStack i = e.getItem();
                if (i != null)
                    if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
                        if (i.getType() == Material.NAME_TAG) {
                            String rawname = i.getItemMeta().getDisplayName();
                            if (i.getItemMeta() != null && rawname.contains("AutoNick")){

                                NickHandler.toggleAutoNick(e.getPlayer());
                                cooldown.add(e.getPlayer().getUniqueId());

                                if(!e.getPlayer().hasPermission("dynexmc.skip.cooldown"))
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            cooldown.remove(e.getPlayer().getUniqueId());

                                        }
                                    }.runTaskLater(Nick.getInstance(), 100);


                            }


                        }
            }else e.getPlayer().sendMessage(Data.prefix+ ChatColor.RED + "Du musst warten bis dein" + ChatColor.YELLOW + " Cooldown " + ChatColor.RED +  "abgelaufen ist!");

        }
    }
}
