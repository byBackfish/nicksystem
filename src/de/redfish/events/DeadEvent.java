package de.redfish.events;

import de.redfish.data.NickHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeadEvent implements Listener {

    @EventHandler
    public void onDead(PlayerDeathEvent e) {
        if (NickHandler.pforce.get ( e.getEntity ( ) ).equals ( true )) {
            e.setDeathMessage ( null );
            NickHandler.pforce.remove ( e.getEntity ( ) );
            e.setKeepInventory(true);
            e.setDroppedExp(0);

        }
    }
}
