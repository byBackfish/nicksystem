package de.redfish.events;

import de.redfish.data.NickEvents;
import org.bukkit.entity.Player;

public class Debugger implements NickEvents {
    @Override
    public void onUnNick(Player p, String name) {
        System.out.println("NICK");
    }

    @Override
    public void onNick(Player p, String name) {
        System.out.println("UNNICK");
    }
}
